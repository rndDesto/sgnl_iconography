import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import styles from './major_icon.scss?inline';
import svgList from '../icons_set';

@customElement('signal-major-icon')
export class SignalMajorIcon extends LitElement {
    static styles = [
        css`
            ${unsafeCSS(styles)}
            :host(:hover) svg path {
                fill: var(--hover-color, #ffffff);
            }
        `
    ];

    @property({ type: String })
    fill?: 'primary' | 'secondary' | 'tritary' | 'valid'  | 'info' | 'warning' | 'error' | 'disable' = 'primary';

    @property({ type: String })
    partName? = '';

    @property({ type: String })
    name = '';

    @property()
    hover: string = '#001A41';

    connectedCallback() {
        super.connectedCallback();
        this.style.setProperty('--hover-color', this.hover);
    }

    render() {
        if (svgList[this.name]) {
            return html` ${svgList[this.name](this.fill, this.partName)}`;
        } else {
            return html`<p>icon not found</p>`;
        }
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-major-icon': SignalMajorIcon;
    }
}
