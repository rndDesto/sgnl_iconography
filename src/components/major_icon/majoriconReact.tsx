import React from "react";
import { SignalMajorIcon } from "./major_icon";
import { createComponent } from "@lit-labs/react";

const SignalMajorIconReact = createComponent({
    tagName: 'signal-major-icon',
    elementClass: SignalMajorIcon,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalMajorIconReact