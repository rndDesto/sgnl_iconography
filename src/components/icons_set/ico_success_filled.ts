import { svg } from "lit";

const ico_success_filled = (fill = '#001A41', partName = 'ico_success_filled') => {
    return svg`
    <svg part="${partName}" width="20" height="20" viewBox="0 0 20 20" fill="${fill}" xmlns="http://www.w3.org/2000/svg" class="icon-${fill}">
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M10 20C14.9698 20 20 16 20 10C20 4 14.9698 0 10 0C5.0302 0 0 4 0 10C0 16 5.0302 20 10 20ZM15.1773 7.9364C15.5288 7.58492 15.5288 7.01508 15.1773 6.6636C14.8258 6.31213 14.256 6.31213 13.9045 6.6636L8.69091 11.8772L6.1364 9.32269C5.78492 8.97122 5.21508 8.97122 4.8636 9.32269C4.51213 9.67417 4.51213 10.244 4.8636 10.5955L8.05451 13.7864C8.40598 14.1379 8.97583 14.1379 9.3273 13.7864L15.1773 7.9364Z"/>
    </svg>
    `;
}

export default ico_success_filled;