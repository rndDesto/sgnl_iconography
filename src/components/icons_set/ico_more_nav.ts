import { svg } from "lit";

const ico_more_nav = (fill = '#001A41', partName = 'ico_more_nav') => {
    return svg`
    <svg part="${partName}" width="24" height="24" viewBox="0 0 24 24" fill="${fill}" class="icon-${fill}" xmlns="http://www.w3.org/2000/svg">
    <circle part="${partName}" cx="12" cy="5" r="2" fill="${fill}"/>
    <circle part="${partName}" cx="12" cy="12" r="2" fill="${fill}"/>
    <circle part="${partName}" cx="12" cy="19" r="2" fill="${fill}"/>
    </svg>
    `;
}

export default ico_more_nav;