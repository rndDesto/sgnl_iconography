import { svg } from "lit";

const ico_check = (fill = '#001A41', partName = 'ico_check') => {
    return svg`
    <svg part="${partName}" width="24" height="24" viewBox="0 0 24 24" fill="${fill}" class="icon-${fill}" xmlns="http://www.w3.org/2000/svg">
    <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M21.5862 4.34229C22.0993 4.8349 22.1409 5.67795 21.6791 6.2253L10.4291 19.5586C10.1997 19.8305 9.87566 19.9899 9.53286 19.9995C9.19005 20.0092 8.8586 19.8681 8.61611 19.6095L2.36612 12.9428C1.87796 12.4221 1.87796 11.5779 2.36612 11.0572C2.85427 10.5365 3.64573 10.5365 4.13388 11.0572L9.45225 16.7301L19.8209 4.44139C20.2827 3.89405 21.0731 3.84967 21.5862 4.34229Z" fill="${fill}"/>
    </svg>

    `;
}

export default ico_check;