import { svg } from "lit";

const ico_placeholder = (fill = '#001A41', partName = 'ico_placeholder') => {
    return svg`
    <svg width="24" height="24" part="${partName}" viewBox="0 0 24 24" fill="${fill}" class="icon-${fill}" xmlns="http://www.w3.org/2000/svg">
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M12 5L4 19H20L12 5ZM12 9.63081L7.97951 16.6667H16.0205L12 9.63081Z" fill="${fill}"/>
    </svg>
    `;
}

export default ico_placeholder;