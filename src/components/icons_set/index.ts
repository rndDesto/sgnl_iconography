import { TemplateResult } from 'lit';
import ico_success_filled from './ico_success_filled'
import ico_apps from './ico_apps'
import ico_close from './ico_close'
import ico_arrow_right from './ico_arrow_right'
import ico_telephone from './ico_telephone'
import ico_internet from './ico_internet'
import ico_sms from './ico_sms'
import ico_camera from './ico_camera'
import ico_placeholder from './ico_placeholder'
import ico_more from './ico_more'
import ico_wifi from './ico_wifi'
import ico_check from './ico_check'
import ico_search from './ico_search'
import ico_more_nav from './ico_more_nav'



type SvgListType = {
    [key: string]: (fill?: string, partName?: string) => TemplateResult;
}

const svgList:SvgListType = {
    ico_success_filled,
    ico_apps,
    ico_close,
    ico_arrow_right,
    ico_telephone,
    ico_internet,
    ico_sms,
    ico_camera,
    ico_placeholder,
    ico_more,
    ico_wifi,
    ico_check,
    ico_search,
    ico_more_nav
  };
  
  export default svgList;