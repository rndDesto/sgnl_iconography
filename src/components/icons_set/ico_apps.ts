import { svg } from "lit";

const ico_apps = (fill = '#001A41', partName = 'ico_apps') => {
    return svg`
    <svg part="${partName}" width="24" height="24" viewBox="0 0 24 24" fill="${fill}" xmlns="http://www.w3.org/2000/svg" class="icon-${fill}">
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M2.52817 6.94299C2.04683 5.94882 2.46255 4.75267 3.45672 4.27133L7.05694 2.52821C8.05111 2.04687 9.24726 2.46259 9.7286 3.45676L11.4717 7.05698C11.9531 8.05115 11.5373 9.2473 10.5432 9.72864L6.94295 11.4718C5.94878 11.9531 4.75263 11.5374 4.27129 10.5432L2.52817 6.94299ZM7.9285 4.32832L4.32828 6.07143L6.07139 9.67165L9.67161 7.92854L7.9285 4.32832Z"/>
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M13.0076 5C13.0076 3.89543 13.903 3 15.0076 3L19.0076 3C20.1121 3 21.0076 3.89543 21.0076 5V9C21.0076 10.1046 20.1121 11 19.0076 11H15.0076C13.903 11 13.0076 10.1046 13.0076 9V5ZM19.0076 5H15.0076V9H19.0076V5Z"/>
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M13.0076 15C13.0076 13.8954 13.903 13 15.0076 13H19.0076C20.1121 13 21.0076 13.8954 21.0076 15V19C21.0076 20.1046 20.1121 21 19.0076 21H15.0076C13.903 21 13.0076 20.1046 13.0076 19V15ZM19.0076 15H15.0076V19H19.0076V15Z"/>
        <path part="${partName}" fill-rule="evenodd" clip-rule="evenodd" d="M3 15C3 13.8954 3.89543 13 5 13H9C10.1046 13 11 13.8954 11 15V19C11 20.1046 10.1046 21 9 21H5C3.89543 21 3 20.1046 3 19L3 15ZM9 15H5V19H9V15Z"/>
    </svg>

    `;
}

export default ico_apps;